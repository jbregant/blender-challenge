<?php


use Phinx\Seed\AbstractSeed;

class CareersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Analista de sistemas',
                'description' => 'Analista de sistemas',
                'status' => 1
            ],
            [
                'name' => 'Analista en Base de Datos',
                'description' => 'Analista en Base de Datos',
                'status' => 1
            ],
            [
                'name' => 'Arquitectura',
                'description' => 'Arquitectura',
                'status' => 1
            ],
            [
                'name' => 'Educacion Fisica',
                'description' => 'Educacion Fisica',
                'status' => 1
            ],
            [
                'name' => 'Dibujo Tecnico',
                'description' => 'Dibujo Tecnico',
                'status' => 0
            ]
        ];

        $careers = $this->table('careers');
        $careers->insert($data)
            ->save();
    }
}
