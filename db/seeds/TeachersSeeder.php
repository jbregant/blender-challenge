<?php


use Phinx\Seed\AbstractSeed;

class TeachersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'first_name' => 'Juan',
                'last_name' => 'Perez',
                'telephone' => '01136212141',
                'birth_date' => '1989-09-30',
                'status' => 1
            ],
            [
                'first_name' => 'Elisa',
                'last_name' => 'Gonzalez',
                'telephone' => '01112345658',
                'birth_date' => '1980-12-07',
                'status' => 1
            ],
            [
                'first_name' => 'Roberto',
                'last_name' => 'Hernandez',
                'telephone' => '123456789',
                'birth_date' => '1992-10-05',
                'status' => 1
            ],
            [
                'first_name' => 'Anahi',
                'last_name' => 'Gutierrez',
                'telephone' => '01136212141',
                'birth_date' => '1989-09-30',
                'status' => 1
            ],
            [
                'first_name' => 'Jorge',
                'last_name' => 'Rodriguez',
                'telephone' => '01112345658',
                'birth_date' => '1980-12-07',
                'status' => 1
            ],
            [
                'first_name' => 'Fernando',
                'last_name' => 'Del toro',
                'telephone' => '123456789',
                'birth_date' => '1992-10-05',
                'status' => 1
            ],
            [
                'first_name' => 'Agustina',
                'last_name' => 'Villacrespo',
                'telephone' => '123456789',
                'birth_date' => '1992-10-05',
                'status' => 0
            ]
        ];

        $teachers = $this->table('teachers');
        $teachers->insert($data)
            ->save();
    }
}
