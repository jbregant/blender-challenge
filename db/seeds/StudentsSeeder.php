<?php


use Phinx\Seed\AbstractSeed;

class StudentsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'first_name' => 'Damian',
                'last_name' => 'Pereyra',
                'telephone' => '01136212141',
                'birth_date' => '1989-09-30',
                'status' => 1
            ],
            [
                'first_name' => 'Javier',
                'last_name' => 'Bregant',
                'telephone' => '01112345658',
                'birth_date' => '1980-12-07',
                'status' => 1
            ],
            [
                'first_name' => 'Ana',
                'last_name' => 'Gonzalez',
                'telephone' => '123456789',
                'birth_date' => '1992-10-05',
                'status' => 0
            ],
            [
                'first_name' => 'Sergio',
                'last_name' => 'Scattone',
                'telephone' => '123456456',
                'birth_date' => '1988-09-26',
                'status' => 1
            ],
            [
                'first_name' => 'Sabrina',
                'last_name' => 'Basteiro',
                'telephone' => '1234513455',
                'birth_date' => '1985-08-21',
                'status' => 1
            ]
        ];

        $students = $this->table('students');
        $students->insert($data)
            ->save();
    }
}
