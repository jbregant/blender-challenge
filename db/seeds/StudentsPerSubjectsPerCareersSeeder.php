<?php


use Phinx\Seed\AbstractSeed;

class StudentsPerSubjectsPerCareersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'subjects_per_careers' => '1',
                'students' => '1',
                'calification' => '',
                'is_approved' => 0,
                'status' => 1
            ],
            [
                'subjects_per_careers' => '2',
                'students' => '2',
                'calification' => '',
                'is_approved' => 0,
                'status' => 1
            ],
            [
                'subjects_per_careers' => '3',
                'students' => '3',
                'calification' => '',
                'is_approved' => 0,
                'status' => 1
            ],
            [
                'subjects_per_careers' => '4',
                'students' => '4',
                'calification' => '',
                'is_approved' => 0,
                'status' => 1
            ]
        ];

        $teachers = $this->table('students_per_subjects_per_careers');
        $teachers->insert($data)
            ->save();
    }
}
