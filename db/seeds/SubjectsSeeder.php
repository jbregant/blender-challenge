<?php


use Phinx\Seed\AbstractSeed;

class SubjectsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Matematica 1',
                'description' => 'Introduccion a la matematica',
                'status' => 1
            ],
            [
                'name' => 'Informatica 1',
                'description' => 'Introduccion a la matematica',
                'status' => 1
            ],
            [
                'name' => 'Gestion de negocios',
                'description' => 'Gestion de negocios',
                'status' => 1
            ],
            [
                'name' => 'Educacion Fisica',
                'description' => 'Educacion Fisica',
                'status' => 1
            ],
            [
                'name' => 'Estructura 1',
                'description' => 'Intoduccion a la estructura de datos',
                'status' => 1
            ],
            [
                'name' => 'Dibujo tecnico',
                'description' => 'Dibujo tecnico',
                'status' => 1
            ],
            [
                'name' => 'Anatomia',
                'description' => 'Anatomia',
                'status' => 1
            ],
            [
                'name' => 'Salud Alimenticia',
                'description' => 'Salud Alimenticia',
                'status' => 1
            ],
            [
                'name' => 'Biologia 1',
                'description' => 'Intoduccion a la biologia',
                'status' => 0
            ]
        ];

        $subjects = $this->table('subjects');
        $subjects->insert($data)
            ->save();
    }
}
