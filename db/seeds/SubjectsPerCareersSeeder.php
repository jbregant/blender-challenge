<?php


use Phinx\Seed\AbstractSeed;

class SubjectsPerCareersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'careers' => '1',
                'subjects' => '2',
                'teachers' => '1',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '1',
                'subjects' => '2',
                'teachers' => '2',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '1',
                'subjects' => '3',
                'teachers' => '3',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '2',
                'subjects' => '1',
                'teachers' => '4',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '2',
                'subjects' => '2',
                'teachers' => '5',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '2',
                'subjects' => '5',
                'teachers' => '6',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '3',
                'subjects' => '1',
                'teachers' => '1',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '3',
                'subjects' => '7',
                'teachers' => '3',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '4',
                'subjects' => '8',
                'teachers' => '3',
                'period' => '1Q 2020',
                'status' => 1
            ],
            [
                'careers' => '4',
                'subjects' => '1',
                'teachers' => '5',
                'period' => '1Q 2020',
                'status' => 1
            ]
        ];

        $subjectsPerCareers = $this->table('subjects_per_careers');
        $subjectsPerCareers->insert($data)
            ->save();
    }
}
