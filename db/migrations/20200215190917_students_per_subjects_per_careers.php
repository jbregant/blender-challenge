<?php


use Phinx\Migration\AbstractMigration;

class StudentsPerSubjectsPerCareers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        if ($this->hasTable('students_per_subjects_per_careers'))
            $this->table('students_per_subjects_per_careers')->drop();

        $studentPerSubjectPerCareer = $this->table('students_per_subjects_per_careers');
        $studentPerSubjectPerCareer->addColumn('subjects_per_careers', 'integer')
            ->addColumn('students', 'integer')
            ->addColumn('calification','string')
            ->addColumn('is_approved','boolean')
            ->addColumn('status', 'boolean')
            ->addForeignKey('students', 'students', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addForeignKey('subjects_per_careers', 'subjects_per_careers', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }
}
