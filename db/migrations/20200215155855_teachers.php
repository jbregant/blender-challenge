<?php


use Phinx\Migration\AbstractMigration;

class Teachers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        if ($this->hasTable('teachers'))
            $this->table('teachers')->drop();

        $teachers = $this->table('teachers');
        $teachers->addColumn('first_name', 'string')
            ->addColumn('last_name', 'string')
            ->addColumn('telephone', 'string')
            ->addColumn('birth_date', 'datetime', ['default' => null])
            ->addColumn('status', 'boolean')
            ->create();
    }
}
