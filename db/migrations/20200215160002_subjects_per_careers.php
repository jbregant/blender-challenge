<?php


use Phinx\Migration\AbstractMigration;

class SubjectsPerCareers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        if ($this->hasTable('subjects_per_careers'))
            $this->table('subjects_per_careers')->drop();

        $subjectsPerCareer = $this->table('subjects_per_careers');
        $subjectsPerCareer->addColumn('careers', 'integer')
            ->addColumn('subjects', 'integer')
            ->addColumn('teachers', 'integer')
            ->addColumn('period', 'string')
            ->addColumn('status', 'boolean')
            ->addForeignKey('careers', 'careers', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addForeignKey('subjects', 'subjects', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addForeignKey('teachers', 'teachers', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
    }
}
