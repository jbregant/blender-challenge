<?php

return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],
        'db' => [
            'host' => 'database',
            'user' => 'blender',
            'pass' => 'blender',
            'dbname' => 'blender'
        ],
        // Monolog settings
        'logger' => [
            'name' => 'blender_challenge',
            'path' => __DIR__ . '/../logs/blender_challenge.log',
        ]
    ]
];
