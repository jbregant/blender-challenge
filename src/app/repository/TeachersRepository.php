<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\TeachersModel;
use PDO;
use Psr\Container\ContainerInterface;


class TeachersRepository extends TeachersModel implements PDOInterface
{
    private $db;

    /**
     * TeachersRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM teachers WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(TeachersModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM teachers")->fetchAll(PDO::FETCH_CLASS, TeachersModel::class);

    }

    /**
     * @param TeachersModel $newStudent
     * @return mixed
     */
    function __save($newStudent)
    {
        $stmt = $this->db->prepare("INSERT INTO teachers (first_name, last_name, telephone, birth_date, status) VALUES (?,?,?,?,?)");
        return $stmt->execute([$newStudent->getFirstName(), $newStudent->getLastName(), $newStudent->getTelephone(), $newStudent->getBirthDate(), true]);
    }

    /**
     * @param TeachersModel $delStudent
     * @return mixed
     */
    function __delete($delStudent)
    {
        $stmt = $this->db->prepare("DELETE FROM teachers WHERE id = ?");
        return $stmt->execute([$delStudent->getId()]);
    }

    /**
     * @param TeachersModel $students
     * @return mixed
     */
    function __update($students)
    {
        $stmt = $this->db->prepare("UPDATE teachers SET first_name = ?, last_name = ?, telephone = ?, birth_date = ?, status = ? WHERE id = ? ");
        return $stmt->execute([$students->getFirstName(), $students->getLastName(), $students->getTelephone(), $students->getBirthDate(), $students->getStatus(), $students->getId()]);
    }
}