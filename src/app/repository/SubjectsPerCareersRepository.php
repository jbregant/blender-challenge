<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\SubjectsPerCareersModel;
use PDO;
use Psr\Container\ContainerInterface;


class SubjectsPerCareersRepository extends SubjectsPerCareersModel implements PDOInterface
{
    private $db;

    /**
     * StudentsRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {

        $stmt = $this->db->prepare("SELECT * FROM subjects_per_careers WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(SubjectsPerCareersModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM subjects_per_careers")->fetchAll(PDO::FETCH_CLASS, SubjectsPerCareersModel::class);

    }

    /**
     * @param SubjectsPerCareersModel $newStudentsPerSubjectsPerCareers
     * @return mixed
     */
    function __save($newStudentsPerSubjectsPerCareers)
    {
        $stmt = $this->db->prepare("INSERT INTO subjects_per_careers (careers, subjects, teachers, period, status) VALUES (?,?,?,?,?)");
        return $stmt->execute([
            $newStudentsPerSubjectsPerCareers->getCareers(),
            $newStudentsPerSubjectsPerCareers->getSubjects(),
            $newStudentsPerSubjectsPerCareers->getTeachers(),
            $newStudentsPerSubjectsPerCareers->getPeriod(),
            true
        ]);
    }

    /**
     * @param SubjectsPerCareersModel $SubjectsPerCareers
     * @return mixed
     */
    function __delete($SubjectsPerCareers)
    {
        $stmt = $this->db->prepare("DELETE FROM subjects_per_careers WHERE id = ?");
        return $stmt->execute([$SubjectsPerCareers->getId()]);
    }

    /**
     * @param SubjectsPerCareersModel $SubjectsPerCareers
     * @return mixed
     */
    function __update($SubjectsPerCareers)
    {
        $stmt = $this->db->prepare("UPDATE subjects_per_careers SET careers = ?, subjects = ?, teachers = ?, period = ?, status = ? WHERE id = ? ");
        return $stmt->execute([
            $SubjectsPerCareers->getCareers(),
            $SubjectsPerCareers->getSubjects(),
            $SubjectsPerCareers->getTeachers(),
            $SubjectsPerCareers->getPeriod(),
            $SubjectsPerCareers->getStatus(),
            $SubjectsPerCareers->getId()
        ]);
    }
}