<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\StudentsModel;
use App\Model\StudentsPerSubjectsPerCareersModel;
use PDO;
use Psr\Container\ContainerInterface;


class StudentsRepository extends StudentsModel implements PDOInterface
{
    private $db;

    /**
     * StudentsRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM students WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(StudentsModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM students")->fetchAll(PDO::FETCH_CLASS, StudentsModel::class);

    }

    /**
     * @param $id
     * @return mixed
     */
    function __findStudentCalifications($id){
        $stmt = $this->db->prepare("SELECT calification FROM students_per_subjects_per_careers WHERE students = ?");
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_CLASS, StudentsPerSubjectsPerCareersModel::class);
    }

    /**
     * @param StudentsModel $newStudent
     * @return mixed
     */
    function __save($newStudent)
    {
        $stmt = $this->db->prepare("INSERT INTO students (first_name, last_name, telephone, birth_date, status) VALUES (?,?,?,?,?)");
        return $stmt->execute([$newStudent->getFirstName(), $newStudent->getLastName(), $newStudent->getTelephone(), $newStudent->getBirthDate(), true]);
    }

    /**
     * @param StudentsModel $delStudent
     * @return mixed
     */
    function __delete($delStudent)
    {
        $stmt = $this->db->prepare("DELETE FROM students WHERE id = ?");
        return $stmt->execute([$delStudent->getId()]);
    }

    /**
     * @param StudentsModel $students
     * @return mixed
     */
    function __update($students)
    {
        $stmt = $this->db->prepare("UPDATE students SET first_name = ?, last_name = ?, telephone = ?, birth_date = ?, status = ? WHERE id = ? ");
        return $stmt->execute([$students->getFirstName(), $students->getLastName(), $students->getTelephone(), $students->getBirthDate(), $students->getStatus(), $students->getId()]);
    }
}