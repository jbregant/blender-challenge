<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\CareersModel;
use PDO;
use Psr\Container\ContainerInterface;


class CareersRepository extends CareersModel implements PDOInterface
{
    private $db;

    /**
     * Career
     * sRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM careers WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(CareersModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM careers")->fetchAll(PDO::FETCH_CLASS, CareersModel::class);

    }

    /**
     * @param CareersModel $newCareers
     * @return mixed
     */
    function __save($newCareers)
    {
        $stmt = $this->db->prepare("INSERT INTO careers (name, description, status) VALUES (?,?,?)");
        return $stmt->execute([$newCareers->getName(), $newCareers->getDescripticn(), true]);
    }

    /**
     * @param CareersModel $delCareers
     * @return mixed
     */
    function __delete($delCareers)
    {
        $stmt = $this->db->prepare("DELETE FROM careers WHERE id = ?");
        return $stmt->execute([$delCareers->getId()]);
    }

    /**
     * @param CareersModel $careers
     * @return mixed
     */
    function __update($careers)
    {
        $stmt = $this->db->prepare("UPDATE careers SET name = ?, description= ?, status = ? WHERE id = ? ");
        return $stmt->execute([$careers->getName(), $careers->getDescripticn(), $careers->getStatus(), $careers->getId()]);
    }
}