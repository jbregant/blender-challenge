<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\StudentsPerSubjectsPerCareersModel;
use PDO;
use Psr\Container\ContainerInterface;


class StudentsPerSubjectsPerCareersRepository extends StudentsPerSubjectsPerCareersModel implements PDOInterface
{
    private $db;

    /**
     * StudentsRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {

        $stmt = $this->db->prepare("SELECT * FROM students_per_subjects_per_careers WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(StudentsPerSubjectsPerCareersModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM students_per_subjects_per_careers")->fetchAll(PDO::FETCH_CLASS, StudentsPerSubjectsPerCareersModel::class);

    }

    /**
     * @param StudentsPerSubjectsPerCareersModel $newStudentsPerSubjectsPerCareers
     * @return mixed
     */
    function __save($newStudentsPerSubjectsPerCareers)
    {
        $stmt = $this->db->prepare("INSERT INTO students_per_subjects_per_careers (subjects_per_careers, students, calification, is_approved, status) VALUES (?,?,?,?,?)");
        return $stmt->execute([
            $newStudentsPerSubjectsPerCareers->getSubjectsPerCareers(),
            $newStudentsPerSubjectsPerCareers->getStudents(),
            $newStudentsPerSubjectsPerCareers->getCalification(),
            $newStudentsPerSubjectsPerCareers->getIsApproved(),
            true
        ]);
    }

    /**
     * @param StudentsPerSubjectsPerCareersModel $studentsPerSubjectsPerCareers
     * @return mixed
     */
    function __delete($studentsPerSubjectsPerCareers)
    {
        $stmt = $this->db->prepare("DELETE FROM students_per_subjects_per_careers WHERE id = ?");
        return $stmt->execute([$studentsPerSubjectsPerCareers->getId()]);
    }

    /**
     * @param StudentsPerSubjectsPerCareersModel $studentsPerSubjectsPerCareers
     * @return mixed
     */
    function __update($studentsPerSubjectsPerCareers)
    {
        $stmt = $this->db->prepare("UPDATE students_per_subjects_per_careers SET subjects_per_careers = ?, students = ?, calification = ?, is_approved = ?, status = ? WHERE id = ? ");
        return $stmt->execute([
            $studentsPerSubjectsPerCareers->getSubjectsPerCareers(),
            $studentsPerSubjectsPerCareers->getStudents(),
            $studentsPerSubjectsPerCareers->getCalification(),
            $studentsPerSubjectsPerCareers->getIsApproved(),
            $studentsPerSubjectsPerCareers->getStatus(),
            $studentsPerSubjectsPerCareers->getId()
        ]);
    }
}