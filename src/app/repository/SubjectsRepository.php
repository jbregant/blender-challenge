<?php

namespace App\Repository;

use App\model\PDOInterface;
use App\Model\SubjectsModel;
use PDO;
use Psr\Container\ContainerInterface;


class SubjectsRepository extends SubjectsModel implements PDOInterface
{
    private $db;

    /**
     * SubjectsRepository constructor.
     * @param ContainerInterface $ci
     */
    public function __construct($ci)
    {
        $this->db = $ci->get('db');
    }

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM subjects WHERE id = ?");
        $stmt->execute([$id]);
        return $stmt->fetchObject(SubjectsModel::class);
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array)
    {
        // TODO: Implement __findBy() method.
    }

    /**
     * @return mixed
     */
    function __findAll()
    {
        return $this->db->query("SELECT * FROM subjects")->fetchAll(PDO::FETCH_CLASS, SubjectsModel::class);

    }

    /**
     * @param SubjectsModel $newSubject
     * @return mixed
     */
    function __save($newSubject)
    {
        $stmt = $this->db->prepare("INSERT INTO subjects (name, description, status) VALUES (?,?,?)");
        return $stmt->execute([$newSubject->getName(), $newSubject->getDescripticn(), true]);
    }

    /**
     * @param SubjectsModel $delSubject
     * @return mixed
     */
    function __delete($delSubject)
    {
        $stmt = $this->db->prepare("DELETE FROM subjects WHERE id = ?");
        return $stmt->execute([$delSubject->getId()]);
    }

    /**
     * @param SubjectsModel $subject
     * @return mixed
     */
    function __update($subject)
    {
        $stmt = $this->db->prepare("UPDATE subjects SET name = ?, description= ?, status = ? WHERE id = ? ");
        return $stmt->execute([$subject->getName(), $subject->getDescripticn(), $subject->getStatus(), $subject->getId()]);
    }
}