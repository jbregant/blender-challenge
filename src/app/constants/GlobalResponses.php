<?php


namespace App\constants;


class GlobalResponses
{
    /**
     * default json response for http ok
     */
    public static $JSON_RESPONSE_OK = [
        'code' => 200,
        'status' => 'OK'
    ];

    /**
     * default json response for created resource
     */
    public static $JSON_RESPONSE_CREATED = [
        'code' => 201,
        'status' => 'OK',
        'message' => 'Resource created successfully'
    ];

    /**
     * default json response for updated resource
     */
    public static $JSON_RESPONSE_UPDATED = [
        'code' => 200,
        'status' => 'OK',
        'message' => 'Resource updated successfully'
    ];

    /**
     * default json response for deleted resource
     */
    public static $JSON_RESPONSE_DELETED = [
        'code' => 200,
        'status' => 'OK',
        'message' => 'Resource deleted successfully'
    ];

    /**
     * default json response for resource not found
     */
    public static $JSON_RESPONSE_NOT_FOUND = [
        'code' => 404,
        'status' => 'FAILED',
        'message' => 'Resource not found'
    ];

    /**
     * default json response for internal server error
     */
    public static $JSON_RESPONSE_INTERNAL_ERROR = [
        'code' => 500,
        'status' => 'FAILED',
        'message' => 'Internal server error'
    ];
}