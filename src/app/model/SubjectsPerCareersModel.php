<?php

namespace App\Model;


use App\Repository\SubjectsPerCareersRepository;
use JsonSerializable;

class SubjectsPerCareersModel implements ModelInterface, JsonSerializable
{
    private $id;

    private $careers;

    private $subjects;

    private $teachers;

    private $period;

    private $status;

    /**
     * StudentsModel constructor.
     * @param null $subjectsPerCareersModel
     */
    public function __construct($subjectsPerCareersModel = null)
    {
        if (!null == $subjectsPerCareersModel) {
            $this->id = $subjectsPerCareersModel['id'];
            $this->careers = $subjectsPerCareersModel['careers'];
            $this->subjects = $subjectsPerCareersModel['subjects'];
            $this->teachers = $subjectsPerCareersModel['teachers'];
            $this->period = $subjectsPerCareersModel['period'];
            $this->status = $subjectsPerCareersModel['status'];
        }
    }

    public function __set($name, $value)
    {
        // do not applyed
    }

    public function getRepository($container)
    {
        return new SubjectsPerCareersRepository($container);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCareers()
    {
        return $this->careers;
    }

    /**
     * @param mixed $careers
     */
    public function setCareers($careers)
    {
        $this->careers = $careers;
    }

    /**
     * @return mixed
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * @param mixed $subjects
     */
    public function setSubjects($subjects)
    {
        $this->subjects = $subjects;
    }

    /**
     * @return mixed
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * @param mixed $teachers
     */
    public function setTeachers($teachers)
    {
        $this->teachers = $teachers;
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param mixed $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}
