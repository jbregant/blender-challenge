<?php

namespace App\Model;


use App\Repository\CareersRepository;
use JsonSerializable;

class CareersModel implements ModelInterface, JsonSerializable
{
    private $id;

    private $name;

    private $description;

    private $status;

    /**
     * SubjectsModel constructor.
     * @param null $careers
     */
    public function __construct($careers = null)
    {
        if (!null == $careers) {
            $this->id = $careers['id'];
            $this->name = $careers['name'];
            $this->description = $careers['description'];
            $this->status = $careers['status'];
        }
    }

    public function __set($name, $value)
    {
        // do not apply
    }

    public function getRepository($container)
    {
        return new CareersRepository($container);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescripticn()
    {
        return $this->description;
    }

    /**
     * @param mixed $descripticn
     */
    public function setDescripticn($descripticn)
    {
        $this->description = $descripticn;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
