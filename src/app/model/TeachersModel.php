<?php

namespace App\Model;


use App\Repository\StudentsRepository;
use App\Repository\TeachersRepository;
use JsonSerializable;

class TeachersModel implements ModelInterface, JsonSerializable
{
    private $id;

    private $firstName;

    private $lastName;

    private $telephone;

    private $birthDate;

    private $status;

    /**
     * TeachersModel constructor.
     * @param null $teachers
     */
    public function __construct($teachers = null)
    {
        if (!null == $teachers) {
            $this->id = $teachers['id'];
            $this->firstName = $teachers['firstName'];
            $this->lastName = $teachers['lastName'];
            $this->telephone = $teachers['telephone'];
            $this->birthDate = $teachers['birthDate'];
            $this->status = $teachers['status'];
        }
    }

    public function __set($name, $value)
    {
        switch ($name) {
            case 'first_name':
                $this->setFirstName($value);
                break;
            case 'last_name':
                $this->setLastName($value);
                break;
            case 'birth_date':
                $this->setBirthDate($value);
                break;
            default:
                break;
        }
    }

    public function getRepository($container)
    {
        return new TeachersRepository($container);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}
