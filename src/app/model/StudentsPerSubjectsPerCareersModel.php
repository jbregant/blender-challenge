<?php

namespace App\Model;


use App\Repository\StudentsPerSubjectsPerCareersRepository;
use App\Repository\StudentsRepository;
use JsonSerializable;

class StudentsPerSubjectsPerCareersModel implements ModelInterface, JsonSerializable
{
    private $id;

    private $subjectsPerCareers;

    private $students;

    private $calification;

    private $isApproved;

    private $status;

    /**
     * StudentsModel constructor.
     * @param null $studentsPerSubjectsPerCareersModel
     */
    public function __construct($studentsPerSubjectsPerCareersModel = null)
    {
        if (!null == $studentsPerSubjectsPerCareersModel) {
            $this->id = $studentsPerSubjectsPerCareersModel['id'];
            $this->subjectsPerCareers = $studentsPerSubjectsPerCareersModel['subjectsPerCareers'];
            $this->students = $studentsPerSubjectsPerCareersModel['students'];
            $this->calification = $studentsPerSubjectsPerCareersModel['calification'];
            $this->isApproved = $studentsPerSubjectsPerCareersModel['isApproved'];
            $this->status = $studentsPerSubjectsPerCareersModel['status'];
        }
    }

    public function __set($name, $value)
    {
        switch ($name) {
            case 'subjects_per_careers':
                $this->setSubjectsPerCareers($value);
                break;
            case 'is_approved':
                $this->setIsApproved($value);
                break;
            default:
                break;
        }
    }

    public function getRepository($container)
    {
        return new StudentsPerSubjectsPerCareersRepository($container);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getSubjectsPerCareers()
    {
        return $this->subjectsPerCareers;
    }

    /**
     * @param mixed $subjectsPerCareers
     */
    public function setSubjectsPerCareers($subjectsPerCareers)
    {
        $this->subjectsPerCareers = $subjectsPerCareers;
    }

    /**
     * @return mixed
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @param mixed $students
     */
    public function setStudents($students)
    {
        $this->students = $students;
    }

    /**
     * @return mixed
     */
    public function getCalification()
    {
        return $this->calification;
    }

    /**
     * @param mixed $calification
     */
    public function setCalification($calification)
    {
        $this->calification = $calification;
    }

    /**
     * @return mixed
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param mixed $isApproved
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}
