<?php


namespace App\model;

/**
 * Interface PDOInterface
 * @package App\model
 */
interface PDOInterface
{

    /**
     * PDOInterface constructor.
     * @param $ci
     */
    function __construct($ci);

    /**
     * @param $obj
     * @return mixed
     */
    function __save($obj);

    /**
     * @param $id
     * @return mixed
     */
    function __delete($id);

    /**
     * @param $obj
     * @return mixed
     */
    function __update($obj);

    /**
     * @param $id
     * @return mixed
     */
    function __findByID($id);

    /**
     * @return mixed
     */
    function __findAll();

    /**
     *
     * @param $array
     * @return mixed
     */
    function __findBy($array);


}