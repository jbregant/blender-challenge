<?php


namespace App\Model;

interface ModelInterface
{
    public function getRepository($container);

    public function __set($name, $value);

    public function jsonSerialize();

}