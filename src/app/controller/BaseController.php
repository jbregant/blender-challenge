<?php


namespace App\Controller;


use Psr\Container\ContainerInterface;

class BaseController
{
    private $ci;
    private $db;

    public function __construct(ContainerInterface $ci) {

        $this->ci = $ci;
        $this->db = $ci->get('db');
    }

    /**
     * @return ContainerInterface
     */
    public function getCi()
    {
        return $this->ci;
    }

}