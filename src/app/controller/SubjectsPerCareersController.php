<?php

namespace App\Controller;

use App\constants\GlobalResponses;
use App\Model\SubjectsPerCareersModel;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Valitron\Validator;

Validator::lang('es');

class SubjectsPerCareersController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function listOne(Request $request, Response $response, $param)
    {
        $parsedUri = $param;

        $v = new Validator($parsedUri);
        $v->rule('required', ['id']);
        $v->rule('numeric', 'id');

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        GlobalResponses::$JSON_RESPONSE_OK['data'] = (new SubjectsPerCareersModel())->getRepository($this->getCi())->__findByID($parsedUri['id']);
        return $response->withJson(GlobalResponses::$JSON_RESPONSE_OK, StatusCode::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function listAll(Request $request, Response $response)
    {
        GlobalResponses::$JSON_RESPONSE_OK['data'] = (new SubjectsPerCareersModel())->getRepository($this->getCi())->__findAll();
        return $response->withJson(GlobalResponses::$JSON_RESPONSE_OK, StatusCode::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function add(Request $request, Response $response)
    {
        $parsedBody = $request->getParsedBody();

        $v = new Validator($parsedBody);
        $v->rule('required', ['careers', 'subjects', 'teachers','period']);
        $v->rule('integer', 'careers');
        $v->rule('integer', 'subjects');
        $v->rule('integer', 'teachers');


        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        $newSubjectsPerCareersModel = new SubjectsPerCareersModel($parsedBody);
        if ($newSubjectsPerCareersModel->getRepository($this->getCi())->__save($newSubjectsPerCareersModel)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_CREATED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function del(Request $request, Response $response, $param)
    {
        $parsedUri = $param;

        $v = new Validator($parsedUri);
        $v->rule('required', ['id']);
        $v->rule('numeric', 'id');

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        $delSubjectPerCareer = new SubjectsPerCareersModel($parsedUri);
        if ($delSubjectPerCareer->getRepository($this->getCi())->__delete($delSubjectPerCareer)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_DELETED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function update(Request $request, Response $response, $param)
    {
        $parsedUri = $param;
        $parsedBody = $request->getParsedBody();
        $parsedTotal = array_merge($parsedUri, $parsedBody);

        $v = new Validator($parsedTotal);
        $v->rule('required', ['careers', 'subjects', 'teachers','period']);
        $v->rule('integer', 'careers');
        $v->rule('integer', 'subjects');
        $v->rule('integer', 'teachers');
        $v->rule('alphaNum', 'period');

        $studentsubjectsPerCareers = new SubjectsPerCareersModel($parsedTotal);

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        if ($studentsubjectsPerCareers->getRepository($this->getCi())->__update($studentsubjectsPerCareers)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_UPDATED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }
}