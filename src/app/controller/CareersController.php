<?php

namespace App\Controller;

use App\constants\GlobalResponses;
use App\Model\CareersModel;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Valitron\Validator;

Validator::lang('es');

class CareersController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function listOne(Request $request, Response $response, $param)
    {
        $parsedUri = $param;

        $v = new Validator($parsedUri);
        $v->rule('required', ['id']);
        $v->rule('numeric', 'id');

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        GlobalResponses::$JSON_RESPONSE_OK['data'] = (new CareersModel())->getRepository($this->getCi())->__findByID($parsedUri['id']);

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_OK, StatusCode::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function listAll(Request $request, Response $response)
    {
        GlobalResponses::$JSON_RESPONSE_OK['data'] = (new CareersModel())->getRepository($this->getCi())->__findAll();
        return $response->withJson(GlobalResponses::$JSON_RESPONSE_OK, StatusCode::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function add(Request $request, Response $response)
    {
        $parsedBody = $request->getParsedBody();

        $v = new Validator($parsedBody);
        $v->rule('required', ['name', 'description']);

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        $newCareer = new CareersModel($parsedBody);
        if ($newCareer->getRepository($this->getCi())->__save($newCareer)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_CREATED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function del(Request $request, Response $response, $param)
    {
        $parsedUri = $param;

        $v = new Validator($parsedUri);
        $v->rule('required', ['id']);
        $v->rule('numeric', 'id');

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        $delCareer = new CareersModel($parsedUri);
        if ($delCareer->getRepository($this->getCi())->__delete($delCareer)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_DELETED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $param
     * @return Response
     */
    public function update(Request $request, Response $response, $param)
    {
        $parsedUri = $param;
        $parsedBody = $request->getParsedBody();
        $parsedTotal = array_merge($parsedUri, $parsedBody);
        $v = new Validator($parsedTotal);
        $v->rule('required', ['name', 'description']);

        $teacher = new CareersModel($parsedTotal);

        if (!$v->validate()) {
            return $response->withJson($v->errors(), StatusCode::HTTP_BAD_REQUEST);
        }

        if ($teacher->getRepository($this->getCi())->__update($teacher)) {
            return $response->withJson(GlobalResponses::$JSON_RESPONSE_UPDATED, StatusCode::HTTP_OK);
        }

        return $response->withJson(GlobalResponses::$JSON_RESPONSE_INTERNAL_ERROR, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }
}