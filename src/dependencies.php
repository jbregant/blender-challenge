<?php
// DIC configuration

use App\Controller\CareersController;
use App\Controller\StudentsController;
use App\Controller\StudentsPerSubjectsPerCareersController;
use App\Controller\SubjectsController;
use App\Controller\SubjectsPerCareersController;
use App\Controller\TeachersController;

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// PDO databases library
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
//    pgsql:host=localhost;port=5432;dbname=pagila;user=postgres;password=postgres
    $pdo = new PDO("pgsql:host=" . $settings['host'] , $settings['user'], $settings['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// Controllers
$container['StudentsController'] = function ($c) {
    return new StudentsController($c);
};

$container['StudentsPerSubjectsPerCareersController'] = function ($c) {
    return new StudentsPerSubjectsPerCareersController($c);
};

$container['SubjectsPerCareersController'] = function ($c) {
    return new SubjectsPerCareersController($c);
};

$container['TeachersController'] = function ($c) {
    return new TeachersController($c);
};

$container['SubjectsController'] = function ($c) {
    return new SubjectsController($c);
};

$container['CareersController'] = function ($c) {
    return new CareersController($c);
};