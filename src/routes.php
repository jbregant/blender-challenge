<?php

use App\Controller\CareersController;
use App\Controller\StudentsController;
use App\Controller\StudentsPerSubjectsPerCareersController;
use App\Controller\SubjectsController;
use App\Controller\SubjectsPerCareersController;
use App\Controller\TeachersController;

// Api Routes Group
$app->group('/api/v1', function () {
    // students group
    $this->group('/students', function () {
        $this->get('/', StudentsController::class .':listAll');
        $this->get('/{id}', StudentsController::class .':listOne');
        $this->get('/califications/{id}', StudentsController::class .':studentCalifications');
        $this->post('/', StudentsController::class .':add');
        $this->delete('/{id}', StudentsController::class .':del');
        $this->patch('/{id}', StudentsController::class .':update');
    });

    $this->group('/calification', function () {
        $this->get('/', StudentsPerSubjectsPerCareersController::class .':listAll');
        $this->get('/{id}', StudentsPerSubjectsPerCareersController::class .':listOne');
        $this->post('/', StudentsPerSubjectsPerCareersController::class .':add');
        $this->delete('/{id}', StudentsPerSubjectsPerCareersController::class .':del');
        $this->patch('/{id}', StudentsPerSubjectsPerCareersController::class .':update');
    });

    $this->group('/subjectpercareer', function () {
        $this->get('/', SubjectsPerCareersController::class .':listAll');
        $this->get('/{id}', SubjectsPerCareersController::class .':listOne');
        $this->post('/', SubjectsPerCareersController::class .':add');
        $this->delete('/{id}', SubjectsPerCareersController::class .':del');
        $this->patch('/{id}', SubjectsPerCareersController::class .':update');
    });

    $this->group('/teachers', function () {
        $this->get('/', TeachersController::class .':listAll');
        $this->get('/{id}', TeachersController::class .':listOne');
        $this->post('/', TeachersController::class .':add');
        $this->delete('/{id}', TeachersController::class .':del');
        $this->patch('/{id}', TeachersController::class .':update');
    });

    $this->group('/subjects', function () {
        $this->get('/', SubjectsController::class .':listAll');
        $this->get('/{id}', SubjectsController::class .':listOne');
        $this->post('/', SubjectsController::class .':add');
        $this->delete('/{id}', SubjectsController::class .':del');
        $this->patch('/{id}', SubjectsController::class .':update');
    });

    $this->group('/careers', function () {
        $this->get('/', CareersController::class .':listAll');
        $this->get('/{id}', CareersController::class .':listOne');
        $this->post('/', CareersController::class .':add');
        $this->delete('/{id}', CareersController::class .':del');
        $this->patch('/{id}', CareersController::class .':update');
    });
});